package coding.test.sixtct

import coding.test.sixtct.service.model.RemoteVehicle
import coding.test.sixtct.view.model.toVehicle
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test

import org.junit.Assert.*

class VehicleMapperUnitTest {

    val remoteVehicle: RemoteVehicle = mock {
        on(it.modelName).thenReturn("model name")
        on(it.name).thenReturn("name")
        on(it.color).thenReturn("color")
        on(it.fuelType).thenReturn("fuel type")
        on(it.fuelLevel).thenReturn(0.5)
        on(it.transmission).thenReturn("transmission")
        on(it.licensePlate).thenReturn("license plate")
        on(it.latitude).thenReturn(3.21)
        on(it.longitude).thenReturn(1.23)
        on(it.innerCleanliness).thenReturn("inner cleanliness")
        on(it.carImageUrl).thenReturn("car image url")
    }

    @Test
    fun `'convert RemoteVehicle to Vehicle'`(){
        remoteVehicle.toVehicle().apply {
            assertEquals(modelName, "model name")
            assertEquals(name, "name")
            assertEquals(color, "color")
            assertEquals(fuelType, "fuel type")
            assertEquals(fuelLevel, "50%")
            assertEquals(transmission, "transmission")
            assertEquals(licensePlate, "license plate")
            assert(latitude == 3.21)
            assert(longitude == 1.23)
            assertEquals(innerCleanliness, "inner cleanliness")
            assertEquals(carImageUrl, "car image url")
        }
    }

    @Test
    fun `'map Automatic transmission'`(){
        whenever(remoteVehicle.transmission).thenReturn("A")
        remoteVehicle.toVehicle().apply {
            assertEquals(transmission, "Automatic")
        }
    }

    @Test
    fun `'map Manual transmission'`(){
        whenever(remoteVehicle.transmission).thenReturn("M")
        remoteVehicle.toVehicle().apply {
            assertEquals(transmission, "Manual")
        }
    }

    @Test
    fun `'map Unknown transmission'`(){
        whenever(remoteVehicle.transmission).thenReturn("Unknown")
        remoteVehicle.toVehicle().apply {
            assertEquals(transmission, "Unknown")
        }
    }

    @Test
    fun `'map Electric "fuel"'`(){
        whenever(remoteVehicle.fuelType).thenReturn("E")
        remoteVehicle.toVehicle().apply {
            assertEquals(fuelType, "Electric")
        }
    }

    @Test
    fun `'map Diesel fuel'`(){
        whenever(remoteVehicle.fuelType).thenReturn("D")
        remoteVehicle.toVehicle().apply {
            assertEquals(fuelType, "Diesel")
        }
    }

    @Test
    fun `'map Petrol fuel'`(){
        whenever(remoteVehicle.fuelType).thenReturn("P")
        remoteVehicle.toVehicle().apply {
            assertEquals(fuelType, "Petrol")
        }
    }

    @Test
    fun `'map Unknown fuel'`(){
        whenever(remoteVehicle.fuelType).thenReturn("Unknown")
        remoteVehicle.toVehicle().apply {
            assertEquals(fuelType, "Unknown")
        }
    }

    @Test
    fun `'map empty fuel level'`(){
        whenever(remoteVehicle.fuelLevel).thenReturn(0.0)
        remoteVehicle.toVehicle().apply {
            assertEquals(fuelLevel, "0%")
        }
    }

    @Test
    fun `'map decimal fuel level'`(){
        whenever(remoteVehicle.fuelLevel).thenReturn(0.321765671)
        remoteVehicle.toVehicle().apply {
            assertEquals(fuelLevel, "32%")
        }
    }

    @Test
    fun `'map full fuel level'`(){
        whenever(remoteVehicle.fuelLevel).thenReturn(1.0)
        remoteVehicle.toVehicle().apply {
            assertEquals(fuelLevel, "100%")
        }
    }

    @Test
    fun `'map underscore named color'`(){
        whenever(remoteVehicle.color).thenReturn("weird_format_color_name")
        remoteVehicle.toVehicle().apply {
            assertEquals(color, "weird format color name")
        }
    }

    @Test
    fun `'map capital underscore named cleanliness'`(){
        whenever(remoteVehicle.innerCleanliness).thenReturn("SUPER_DIRTY_CAR")
        remoteVehicle.toVehicle().apply {
            assertEquals(innerCleanliness, "super dirty car")
        }
    }


}
