package coding.test.sixtct.utils.extensions

import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.removeDecorations() {
    (0 until itemDecorationCount).forEach { removeItemDecorationAt(it) }
}