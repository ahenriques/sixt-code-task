package coding.test.sixtct

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import coding.test.sixtct.view.MapsPresenter
import coding.test.sixtct.view.VehicleViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker


class MapsFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    lateinit var map: GoogleMap

    lateinit var presenter: MapsPresenter

    val safeArgs: MapsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewContent = inflater.inflate(R.layout.fragment_map, container, false)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        initPresenter(viewContent)
        mapFragment.getMapAsync(this)
        return viewContent
    }

    fun initPresenter(viewContent: View) {
        activity?.let {
            presenter = MapsPresenter(
                viewContent,
                ViewModelProviders.of(it).get(VehicleViewModel::class.java)
            )
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        val myTaxiHQ = LatLng(SIXT_HQ_LAT, SIXT_HQ_LNG)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(myTaxiHQ, CITY_LEVEL_ZOOM))
        map.setOnMarkerClickListener(this)
        if(::presenter.isInitialized)
            presenter.subscribeLiveData(this, map, safeArgs)
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        if(::presenter.isInitialized) {
            presenter.handleMarkerClick(marker)
        }
        return false
    }

    companion object {
        const val SIXT_HQ_LAT = 48.043184
        const val SIXT_HQ_LNG = 11.511493

        const val CITY_LEVEL_ZOOM = 10f
        const val STREET_LEVEL_ZOOM = 15f
    }
}