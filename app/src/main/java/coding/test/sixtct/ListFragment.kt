package coding.test.sixtct

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import coding.test.sixtct.view.ListPresenter
import coding.test.sixtct.view.VehicleViewModel

class ListFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewContent = inflater.inflate(R.layout.fragment_car_list, container, false)
        subscribeUI(viewContent)
        return viewContent
    }

    fun subscribeUI(viewContent: View) {
        activity?.let {
            ListPresenter(
                viewContent,
                ViewModelProviders.of(it).get(VehicleViewModel::class.java)
            ).subscribeLiveData(this)
        }
    }

}