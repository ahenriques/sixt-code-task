package coding.test.sixtct.service

import android.content.Context
import coding.test.sixtct.R
import coding.test.sixtct.service.model.RemoteVehicle
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.BufferedReader
import java.io.InputStreamReader


object VehiclesRepository {

    fun getVehicles(context: Context, resultsCallback: (positions: List<RemoteVehicle>) -> Unit) {
        val raw = context.resources.openRawResource(R.raw.sixt_fake_data)
        val rd = BufferedReader(InputStreamReader(raw))
        val vehiclesType = object : TypeToken<List<RemoteVehicle>>() {}.type

        val vehicles = Gson().fromJson<List<RemoteVehicle>>(rd, vehiclesType)
        resultsCallback(vehicles)
    }

}