package coding.test.sixtct.view

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import coding.test.sixtct.MapsFragment
import coding.test.sixtct.MapsFragmentArgs
import coding.test.sixtct.view.model.Vehicle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.bottom_sheet_car_detail.view.*

class MapsPresenter(val view: View, val viewModel: VehicleViewModel) {

    init {
        view.bottom_sheet.visibility = View.GONE
    }

    val sheetPresenter = SheetPresenter(view.bottom_sheet, viewModel)

    fun subscribeLiveData(lifecycleOwner: LifecycleOwner, map: GoogleMap, safeArgs: MapsFragmentArgs) {
        viewModel.vehicles.observe(lifecycleOwner, Observer { markersList ->
            map.clear()
            val markersHash = HashMap<String, Marker>()

            markersList.forEach {
                val marker = map.addMarker(viewModel.createMarkerOptionsForVehicle(it))
                markersHash.put(it.licensePlate, marker)
            }

            viewModel.vehicleMarkers.postValue(markersHash)
            focusOnVehicle(safeArgs.vehicle, map)
        })
        sheetPresenter.subscribeLiveData(lifecycleOwner)
    }

    private fun focusOnVehicle(vehicle: Vehicle?, map: GoogleMap) {
        vehicle?.apply {
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(latitude, longitude),
                    MapsFragment.STREET_LEVEL_ZOOM
                )
            )
        }
    }

    fun handleMarkerClick(marker: Marker) {
        viewModel.selectVehicle(marker.title)
    }

}