package coding.test.sixtct.view.model

import coding.test.sixtct.service.model.RemoteVehicle

fun RemoteVehicle.toVehicle() =
    Vehicle(
        modelName = this.modelName,
        name = this.name,
        color = this.color.replace("_", " "),
        fuelType = mapFuel(this.fuelType),
        fuelLevel = formatDoubleToPercentage(this.fuelLevel),
        transmission=  mapTransmission(this.transmission),
        licensePlate = this.licensePlate,
        latitude = this.latitude,
        longitude = this.longitude,
        innerCleanliness = this.innerCleanliness.replace("_", " ").toLowerCase(),
        carImageUrl = this.carImageUrl
    )

private fun mapFuel(fuelType: String): String {
    return when(fuelType) {
        "D" -> "Diesel"
        "E" -> "Electric"
        "P" -> "Petrol"
        else -> fuelType
    }
}

private fun formatDoubleToPercentage(double: Double): String {
    return "${String.format("%.0f", double*100)}%"
}

private fun mapTransmission(transmissionType: String): String {
    return when(transmissionType) {
        "A" -> "Automatic"
        "M" -> "Manual"
        else -> transmissionType
    }
}

