package coding.test.sixtct.view

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import coding.test.sixtct.R
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.bottom_sheet_car_detail.view.*



class SheetPresenter(private val view: View, private val viewModel: VehicleViewModel) {

    init {
        view.setOnClickListener { toggleBottomSheet() }
    }

    private val sheetBehavior = BottomSheetBehavior.from(view)

    fun subscribeLiveData(lifecycleOwner: LifecycleOwner) = with(view) {
        viewModel.selectedVehicle.observe(lifecycleOwner, Observer { vehicle ->
            Glide.with(context)
                .load(vehicle.carImageUrl)
                .error(R.drawable.ic_car)
                .fitCenter()
                .into(carPicture)
            view.visibility = View.VISIBLE
            licensePlate.text = context.getString(R.string.list_license_plate, vehicle.licensePlate)
            name.text = context.getString(R.string.list_name,vehicle.name)
            color.text = context.getString(R.string.list_color,vehicle.color)
            modelName.text = context.getString(R.string.list_model_name,vehicle.modelName)
            innerCleanliness.text = context.getString(R.string.list_inner_cleanliness,vehicle.innerCleanliness)
            fuelType.text = context.getString(R.string.list_fuel_type,vehicle.fuelType)
            fuelLevel.text = context.getString(R.string.list_fuel_level,vehicle.fuelLevel)
            transmissionType.text = context.getString(R.string.list_transmission_type,vehicle.transmission)
        })
    }

    private fun openBottomSheet() {
        sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun closeBottomSheet() {
        sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    fun toggleBottomSheet() {
        if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            openBottomSheet()
        } else {
            closeBottomSheet()
        }
    }
}