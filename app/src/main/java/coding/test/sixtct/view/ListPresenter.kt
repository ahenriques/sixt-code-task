package coding.test.sixtct.view

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import coding.test.sixtct.ListFragmentDirections
import coding.test.sixtct.R
import coding.test.sixtct.utils.MarginItemDecoration
import coding.test.sixtct.utils.extensions.removeDecorations
import kotlinx.android.synthetic.main.fragment_car_list.view.*

class ListPresenter(private val view: View, private val viewModel: VehicleViewModel) {

    fun subscribeLiveData(lifecycleOwner: LifecycleOwner) = with(view){
        viewModel.vehicles.observe(lifecycleOwner,
            Observer { positions ->
                positions?.let {
                    list.apply {

                        adapter = ListAdapter(it, onClick = {
                            viewModel.selectedVehicle.postValue(it)
                            findNavController().navigate(
                                ListFragmentDirections.actionListDestToMapsDest(it)
                            )
                        })

                        removeDecorations()
                        addItemDecoration(MarginItemDecoration(context.resources.getDimension(R.dimen.margin_big).toInt()))
                    }
                }
            }
        )
    }

}