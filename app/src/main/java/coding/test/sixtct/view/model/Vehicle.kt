package coding.test.sixtct.view.model

import java.io.Serializable

data class Vehicle (
    val modelName: String,
    val name: String,
    val color: String,
    val fuelType: String,
    val fuelLevel: String,
    val transmission: String,
    val licensePlate: String,
    val latitude: Double,
    val longitude: Double,
    val innerCleanliness: String,
    val carImageUrl: String
) : Serializable