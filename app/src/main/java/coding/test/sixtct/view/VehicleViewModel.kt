package coding.test.sixtct.view

import android.app.Application
import androidx.lifecycle.*
import coding.test.sixtct.service.VehiclesRepository
import coding.test.sixtct.view.model.Vehicle
import coding.test.sixtct.view.model.toVehicle
import com.google.android.gms.maps.model.*

class VehicleViewModel(application: Application) : AndroidViewModel(application) {

    val vehicles = MutableLiveData<List<Vehicle>>()

    val selectedVehicle = MutableLiveData<Vehicle>()

    init {
        getNewData()
    }

    val vehicleMarkers = MutableLiveData<HashMap<String, Marker>>()

    fun createMarkerOptionsForVehicle(vehicle: Vehicle) =
        MarkerOptions()
            .position(LatLng(vehicle.latitude, vehicle.longitude))
            .title(vehicle.licensePlate)

            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
            .flat(true)


    fun getNewData() {
        VehiclesRepository.getVehicles(getApplication()) { vehicles -> this.vehicles.postValue(vehicles.map { it.toVehicle() }) }
    }

    fun selectVehicle(licensePlate: String) {
        selectedVehicle.postValue(vehicles.value?.first { it.licensePlate == licensePlate })
    }

}