package coding.test.sixtct.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coding.test.sixtct.R
import coding.test.sixtct.view.model.Vehicle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_car_position.view.*

class ListAdapter(private val vehiclesList: List<Vehicle>, val onClick: (vehicle: Vehicle) -> Unit): RecyclerView.Adapter<VehicleViewHolder>() {
    override fun getItemCount() = vehiclesList.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder {
        val vehiclePositionView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_car_position, parent, false)

        return VehicleViewHolder(vehiclePositionView)
    }

    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int) {
        val vehicle = vehiclesList[position]
        holder.bindView(vehicle)
        holder.itemView.setOnClickListener { onClick(vehicle) }
    }
}

class VehicleViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bindView(vehicle: Vehicle) = with(itemView) {

        Glide.with(context)
            .load(vehicle.carImageUrl)
            .error(R.drawable.ic_car)
            .fitCenter()
            .into(fleetType)
        modelName.text = vehicle.modelName
        licensePlate.text = vehicle.licensePlate
        fuelType.text = context.getString(R.string.list_fuel_type, "${vehicle.fuelType} (${vehicle.fuelLevel})")
        transmissionType.text = context.getString(R.string.list_transmission_type, vehicle.transmission)
    }

}