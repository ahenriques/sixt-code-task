package coding.test.sixtct


import androidx.test.InstrumentationRegistry
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnHolderItem
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToHolder
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import coding.test.sixtct.service.model.RemoteVehicle
import coding.test.sixtct.view.VehicleViewHolder
import coding.test.sixtct.view.model.Vehicle
import coding.test.sixtct.view.model.toVehicle
import kotlinx.android.synthetic.main.item_car_position.view.*
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.concurrent.thread


@LargeTest
@RunWith(AndroidJUnit4::class)
class MapsActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MapsActivity::class.java)

    val firstVehicle = RemoteVehicle(
        id = "WMWSW31030T222518",
        modelIdentifier = "mini",
        modelName = "MINI",
        name = "Vanessa",
        make = "BMW",
        group = "MINI",
        color = "midnight_black",
        series = "MINI",
        fuelType = "D",
        fuelLevel = 0.7,
        transmission = "M",
        licensePlate = "M-VO0259",
        latitude = 48.134557,
        longitude = 11.576921,
        innerCleanliness = "REGULAR",
        carImageUrl = "https://cdn.sixt.io/codingtask/images/mini.png"
    ).toVehicle()

    @Test
    fun clickOnFirstCarAndSeeDetailsInBottomSheet() {
        onView(withId(R.id.list))
            .perform(scrollToHolder(withVehicle(firstVehicle)), actionOnHolderItem(withVehicle(firstVehicle), click()))
        onView(withId(R.id.licensePlate)).perform(click())
        checkVehicleDetailsOnBottomSheet(firstVehicle)
    }

    private fun checkVehicleDetailsOnBottomSheet(vehicle: Vehicle) {
        val context = InstrumentationRegistry.getTargetContext()

        onView(withId(R.id.carPicture)).check(matches(isDisplayed()))
        onView(withId(R.id.licensePlate)).check(matches(withText(context.getString(R.string.list_license_plate, vehicle.licensePlate))))
        onView(withId(R.id.name)).check(matches(withText(context.getString(R.string.list_name,vehicle.name))))
        onView(withId(R.id.color)).check(matches(withText(context.getString(R.string.list_color,vehicle.color))))
        onView(withId(R.id.modelName)).check(matches(withText(context.getString(R.string.list_model_name,vehicle.modelName))))
        onView(withId(R.id.innerCleanliness)).check(matches(withText(context.getString(R.string.list_inner_cleanliness,vehicle.innerCleanliness))))
        onView(withId(R.id.fuelType)).check(matches(withText(context.getString(R.string.list_fuel_type,vehicle.fuelType))))
        onView(withId(R.id.fuelLevel)).check(matches(withText(context.getString(R.string.list_fuel_level,vehicle.fuelLevel))))
        onView(withId(R.id.transmissionType)).check(matches(withText(context.getString(R.string.list_transmission_type,vehicle.transmission))))
    }

    private fun withVehicle(vehicle: Vehicle): Matcher<VehicleViewHolder> {
        return object : TypeSafeMatcher<VehicleViewHolder>() {
            override fun matchesSafely(vehicleHolder: VehicleViewHolder): Boolean {
                val context = InstrumentationRegistry.getTargetContext()
                with(vehicleHolder.itemView) {
                    return (licensePlate.text.toString() == vehicle.licensePlate)
                            && (modelName.text.toString() == vehicle.modelName)
                            && (fuelType.text.toString() == context.getString(R.string.list_fuel_type, "${vehicle.fuelType} (${vehicle.fuelLevel})"))
                            && (transmissionType.text.toString() == context.getString(R.string.list_transmission_type, vehicle.transmission))
                }
            }

            override fun describeTo(description: Description) {
                description.appendText("could not find an item where:\n")

                description.appendText("the license plate is ${vehicle.licensePlate}\n")
                description.appendText("the model name is ${vehicle.modelName}\n")
                description.appendText("the fuel type is ${vehicle.fuelType} (${vehicle.fuelLevel})\n")
                description.appendText("the transmission type is ${vehicle.transmission}")
            }
        }
    }

}
